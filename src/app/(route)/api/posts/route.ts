import { NextRequest, NextResponse } from 'next/server';
import prisma from '../../../_lib/prisma';
export async function GET() {
    const posts = await prisma.post.findMany({
      /*   where: { published: true }, */
        include: {
          author: {
            select: { name: true },
          },
        },
      });

    return NextResponse.json(posts);
}

export async function POST(request: NextRequest) {
  const body = await request.json();
  const { title, content,published ,authorId} = body;
 
  const newPost = await prisma.post.create({
    data: {
      title: title,
      content: content,
      author: { connect: { id:  authorId } },
     
    },
    include: {
      author: true,
    },
  });
  return NextResponse.json({"success":1,"message":"create success","post":newPost});
}

